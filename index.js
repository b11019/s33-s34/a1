// Express is a unopinionated web application framework written in JS and hosted in Node.js

// *simplify the web dev process.
    // Convention over configuration

// Allows devs to focus on the business logic of their app.
// comes in two forms opinionated or non

// Opinionated Web Framework
    // Speeds up the start up process of the app development

    // Enforces best practices for the framework's use case
    // Lack of flexibility could be a drawback when apps' needs are not available

// unopinionated Web Framework

// Dev dictates how to use the framework
// Offers flexibility to create an application unbound by any use case
// No "right way" of strcuturing an application
// Abundance of options may be overwhelming



// Advantages of Express over plain Node.js
// Simplicity makes it easy to learn and use
// Offers ready to use components on the web
// Adds easier routing and processing of HTTP METHODS.



const express = require('express');

//  app is our server 
// This creates an application that uses exress and store it as app
const app = express();

const port = 4000;



// middleware 
app.use(express.json());
// console.log(ok)
// console.log(`fsds`);
    // running the methods under express
    
// mock data


let users = [
    {
    username : "BMadrigal",
    email: "fateReader@gmail.com",
    password : "dontTalkAboutMe"
    },
    {
        username : "Luisa",
        email: "stronggirl@gmail.com",
        password : "pressure"
    }
];

let items = [
    {
        name: "roses",
        price : 170,
        isActive : true
    },
    {
        name: "tulips",
        price: 250,
        isActive : true
    }
];

// app.get(<endpoint>,function for req & res)

app.get('/', (req, res)=> {

    res.send(`Hello from my first expressJS API`)
    // res.status (200).send()ssss
});

app.get('/greetings', (req,res)=> {
    res.send(`Hello from Batch182-Golingco`)
})
    
app.get('/users', (req, res)=>{
    res.send(users);
    // res.json(users); for specificity
})

app.post('/users', (req, res)=>{
    console.log(req.body);

    let newUser = {
        username : req.body.username,
        email : req.body.email,
        password : req.body.password
    }
    users.push(newUser);
    console.log(users);
    res.send(users);
    // console.log(`adas`)
});

app.delete('/users', (req,res)=>{
    users.pop();
    console.log(users);
    res.send(users);
})

// :index - wildcard 
// update users pass
 app.put('/users/:index', (req,res)=>{
        console.log(req.body);
        console.log(req.params)

        // parseInt the value of the number coming frm req.params
        let index = parseInt(req.params.index);

        // users[0].password
        users[index].password = req.body.password;

        res.send(users[index]);
 })
 app.put ('/user/:index', (req,res)=>{
     console.log(req.body);
     console.log(req.params);
    let index = parseInt(req.params.index);
    users[index].username = req.body.username;
    res.send(users[index])
 })

app.get('/users/getSingleUser/:index', (req,res)=>{
console.log(req.params);
let index = parseInt(req.params.index)
console.log(index);

res.send(users[index]);
console.log(users[index]);

})

//  >> A C T I V I T Y <<  \\

// RETRIEVE
app.get('/items', (req,res)=>{
    res.send(items)
})

// CREATE ITEM
app.post('/items', (req,res)=>{
    console.log(req.body);
    let newItems = {
        name : req.body.name,
        price : req.body.price,
        isActive : req.body.isActive

    };
    items.push(newItems);
    console.log(items);
    res.send(items);


})

app.put('/items/:index', (req,res)=>{
    console.log(req.body)
    console.log(req.params);
    let index = parseInt(req.params.index)
    console.log(index)

    items[index].price = req.body.price
    res.send(items[index]);
    console.log(items[index])
})




// >> A C T I V I T Y No.2 << \\


/*
Activity #2
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
    
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to de-activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
        -send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client

>> Create a new collection in Postman called s34-activity
>> Save the Postman collection in your s33-s34 folder

*/


// S O L U T I O N

// New Route for /items/GetSingleItem/:index

app.get('/items/getSingleItems/:index',(req,res)=>{
    console.log(req.params)
    let index = parseInt(req.params.index);
    console.log(index)
    res.send(items[index]);
    console.log(items[index]);
})


// for archiving

app.put('/items/archive/:index', (req,res)=>{
    console.log(req.params)
    let index = parseInt(req.params.index)
    items[index].isActive = false
    res.send(items[index]);
    console.log(items[index]);
})


// updating back to true

app.put('/items/activate/:index', (req,res)=>{
    console.log(req.params)
    let index = parseInt(req.params.index)
    items[index].isActive = true
    res.send(items[index]);
    console.log(items[index]);
})
app.listen(port, () => console.log(`Server is running at port ${port} and brought to you by ExpressJS `))
